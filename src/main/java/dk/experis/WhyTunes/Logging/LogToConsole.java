package dk.experis.WhyTunes.Logging;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogToConsole {

    // Function for logging messages to the console
    public void log(String message){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + " : " + message);
    }

}
