package dk.experis.WhyTunes.Data_access;

import dk.experis.WhyTunes.Logging.LogToConsole;
import dk.experis.WhyTunes.Models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ViewRepository {

    // Setup
    private final LogToConsole logger = new LogToConsole();
    private final String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    // Function for getting five random tracks from the database
    public ArrayList<Track> selectRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<>();
        try{
            // Open connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Prepare statement
            PreparedStatement prepareStatement =
                    conn.prepareStatement("SELECT TrackId, Name FROM Track ORDER BY RANDOM() LIMIT 5");

            // Execute statement
            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while (resultSet.next()){
                tracks.add(
                        new Track(
                                resultSet.getInt("TrackId"),
                                resultSet.getString("Name")
                        )
                );
            }
            logger.log("Select 5 random tracks successful");

        } catch (Exception ex) {
            logger.log(ex.toString());
        } finally {
            try {
                // Close connection
                conn.close();
            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return tracks;

    }

    // Function for getting five random artists from the database
    public ArrayList<Artist> selectRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try{
            // Open connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Prepare statement
            PreparedStatement prepareStatement =
                    conn.prepareStatement("SELECT ArtistId, Name FROM Artist ORDER BY RANDOM() LIMIT 5");

            // Execute statement
            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while (resultSet.next()){
                artists.add(
                        new Artist(
                                resultSet.getInt("ArtistId"),
                                resultSet.getString("Name")
                        )
                );
            }
            logger.log("Select 5 random artists successful");

        } catch (Exception ex) {
            logger.log(ex.toString());
        } finally {
            try {
                // Close connection
                conn.close();
            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return artists;

    }

    // Function for getting five random genres from the database
    public ArrayList<ViewGenre> selectRandomGenres(){
        ArrayList<ViewGenre> genres = new ArrayList<>();
        try{
            // Open connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Prepare statement
            PreparedStatement prepareStatement =
                    conn.prepareStatement("SELECT GenreId, Name FROM Genre ORDER BY RANDOM() LIMIT 5");

            // Execute statement
            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while (resultSet.next()){
                genres.add(
                        new ViewGenre(
                                resultSet.getInt("GenreId"),
                                resultSet.getString("Name")
                        )
                );
            }
            logger.log("Select 5 random genres successful");

        } catch (Exception ex) {
            logger.log(ex.toString());
        } finally {
            try {
                // Close connection
                conn.close();
            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return genres;
    }

    // Function for searching for tracks from an input string
    public ArrayList<ViewTrack> getTracksByName(String trackName){
        ArrayList<ViewTrack> tracksFound = new ArrayList<>();
        try{
            // Open connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Prepare statement
            PreparedStatement prepareStatement = conn.prepareStatement("Select Track.Name as trackName, A.Title as albumTitle ,A2.Name as artistName, G.Name as genreName from Track inner join Album A on Track.AlbumId = A.AlbumId inner join\n" +
                    "Artist A2 on A.ArtistId = A2.ArtistId inner join Genre G on Track.GenreId = G.GenreId where Track.Name LIKE ?");

            prepareStatement.setString(1, "%" + trackName + "%");

            // Execute query
            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while(resultSet.next()){
                tracksFound.add(
                        new ViewTrack(
                                resultSet.getString("TrackName"),
                                resultSet.getString("ArtistName"),
                                resultSet.getString("AlbumTitle"),
                                resultSet.getString("GenreName")
                        )
                );
            }
            logger.log("Getting tracks based of name successful");

        } catch (Exception ex) {
            logger.log(ex.toString());

        } finally {
            try{
                // Close connection
                conn.close();

            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }

        return tracksFound;
    }
}
