package dk.experis.WhyTunes.Data_access;

import dk.experis.WhyTunes.Logging.LogToConsole;
import dk.experis.WhyTunes.Models.*;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.*;

public class CustomerRepository {

    // Setup
    private final LogToConsole logger = new LogToConsole();
    private final String URL = ConnectionHelper.CONNECTION_URL;
    Connection conn = null;

    // Function for getting all the customers from the database
    public ArrayList<Customer> selectAllCustomers () {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            logger.log("Select all customers successful");

        } catch (Exception ex) {
            logger.log(ex.toString());
        } finally {
            try {
                // Close Connection
                conn.close();
            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return customers;
    }

    // Function for getting a specific customer, from the database, from an input ID
    public Customer getCustomerById(String customerId){
        Customer customer = null;
        try {
            // Open Connection
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1, customerId); // Corresponds to 1st '?' (must match type)
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }
            logger.log("Select specific customer successful");

        } catch (Exception ex){
            logger.log(ex.toString());

        } finally {
            try {
                // Close Connection
                conn.close();

            } catch (Exception ex){
                logger.log(ex.toString());

            }
        }
        return customer;
    }

    // Function for adding a customer to the database
    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try{
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(FirstName,LastName,Country,PostalCode,Phone,Email) VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1,customer.getFirstName());
            preparedStatement.setString(2,customer.getLastName());
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setString(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhoneNumber());
            preparedStatement.setString(6,customer.getEmail());

            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Add customer successful");

        } catch (Exception ex){
            logger.log(ex.toString());

        } finally {
            try {
                // Close Connection
                conn.close();

            } catch (Exception ex){
                logger.log(ex.toString());

            }
        }
        return success;

    }

    // Function for updating an existing customer in the database
    public Boolean updateCustomer(Customer customer) {
        Boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhoneNumber());
            preparedStatement.setString(7,customer.getEmail());
            preparedStatement.setInt(8, customer.getCustomerId());

            // Execute query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            logger.log("Update customer successful");

        } catch (Exception ex){
            logger.log(ex.toString());

        } finally {
            try {
                conn.close();

            } catch (Exception ex){
                logger.log(ex.toString());

            }
        }
        return success;

    }

    // Function for getting the most customers in every country, from the database
    public ArrayList<CustomerCountry> getCustomersInCountry() {
        ArrayList<CustomerCountry> customerCountries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Make SQL query
            PreparedStatement prepareStatement =
                    conn.prepareStatement("SELECT COUNT(CustomerId), Country FROM customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC");

            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while(resultSet.next()){
                customerCountries.add(
                        new CustomerCountry(
                                resultSet.getString("Country"),
                                resultSet.getInt("COUNT(CustomerId)")
                        ));
            }

            logger.log("Get amount of customers in country successful");

        } catch (Exception ex) {
            logger.log(ex.toString());

        } finally {
            try {
                // Close connection
                conn.close();

            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return customerCountries;
    }

    // Function for getting the highest spending customers from the database
    public ArrayList<CustomerSpending> getSpendingByCustomer() {
        ArrayList<CustomerSpending> customerSpendings = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Make SQL query
            PreparedStatement prepareStatement =
                    conn.prepareStatement("SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS SPENDING\n" +
                    "FROM Invoice\n" +
                    "INNER JOIN Customer ON Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.CustomerId ORDER BY SPENDING DESC;");

            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            while(resultSet.next()){
                customerSpendings.add(
                        new CustomerSpending(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getDouble("SPENDING")
                        ));
            }
            logger.log("Get total spendings per customer successful");

        } catch (Exception ex) {
            logger.log(ex.toString());

        } finally {
            try {
                // Close connection
                conn.close();

            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }

        return customerSpendings;
    }

    // Function for getting a specific customers favorite genres from the database
    public CustomerGenre getCustomersFavoriteGenre(String customerId) {
            CustomerGenre customerGenre = null;
        try{
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established");

            // Make SQL query
            PreparedStatement prepareStatement = conn.prepareStatement("SELECT CustomerId, Name, sum FROM\n" +
                    "    (SELECT Customer.CustomerId, Genre.Name, COUNT(Genre.Name) AS sum\n" +
                    "     FROM Customer\n" +
                    "              INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId\n" +
                    "              INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId\n" +
                    "              INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId\n" +
                    "              INNER JOIN Genre ON Genre.GenreId = Track.GenreId WHERE Customer.CustomerId = ? GROUP BY Genre.Name ORDER BY sum DESC)\n" +
                    "              WHERE sum = (SELECT MAX(sum) FROM (SELECT Genre.Name, COUNT(Genre.Name) AS sum\n" +
                    "                    FROM Customer\n" +
                    "                    INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId\n" +
                    "                    INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId\n" +
                    "                    INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId\n" +
                    "                    INNER JOIN Genre ON Genre.GenreId = Track.GenreId WHERE Customer.CustomerId = ? GROUP BY Genre.Name ORDER BY sum DESC)\n" +
                    ")");

            prepareStatement.setString(1,customerId);
            prepareStatement.setString(2,customerId);

            // Execute query
            ResultSet resultSet = prepareStatement.executeQuery();

            // Process results
            if(resultSet.next()){
                        customerGenre = new CustomerGenre(
                                resultSet.getInt("CustomerId"),
                                new ArrayList<Genre>()
                                );
            }
            do{
                customerGenre.addGenre(new Genre(
                        resultSet.getString("Name"),
                        resultSet.getInt("sum")
                ));
            } while (resultSet.next());

        } catch (Exception ex) {

            logger.log(ex.toString());
        } finally {
            try {

                // Close connection
                conn.close();
            } catch (Exception ex) {
                logger.log(ex.toString());
            }
        }
        return customerGenre;
    }

}

