package dk.experis.WhyTunes.Controllers;

import dk.experis.WhyTunes.Data_access.CustomerRepository;
import dk.experis.WhyTunes.Models.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
public class CustomerController {

    private final CustomerRepository crep = new CustomerRepository();

    // Gets all customers
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return crep.selectAllCustomers();
    }

    // Gets a specific customer depending on input ID
    @RequestMapping(value = "api/customer/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable String id){
        return crep.getCustomerById(id);
    }

    // Adds a new customer
    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return crep.addCustomer(customer);
    }

    // Updates existing customer
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        return crep.updateCustomer(customer);
    }

    // Get most customers in each country
    @RequestMapping(value = "api/customer/country", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getCustomersInCountry(){
        return crep.getCustomersInCountry();
    }

    // Get highest spending customers
    @RequestMapping(value = "api/customer/spending", method = RequestMethod.GET)
    public ArrayList<CustomerSpending> getSpendingByCustomer(){
        return crep.getSpendingByCustomer();
    }

    // Get a customers favorite genre
    @RequestMapping(value = "api/customer/genre/{id}", method = RequestMethod.GET)
    public CustomerGenre getCustomersFavoriteGenre(@PathVariable String id){
        return crep.getCustomersFavoriteGenre(id);
    }
}
