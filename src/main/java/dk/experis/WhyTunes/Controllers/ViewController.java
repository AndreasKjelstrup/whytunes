package dk.experis.WhyTunes.Controllers;

import dk.experis.WhyTunes.Data_access.ViewRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {

    ViewRepository vRep = new ViewRepository();

    // Fetches homepage, where 5 random tracks, artists and genres are shown
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("tracks", vRep.selectRandomTracks());
        model.addAttribute("artists", vRep.selectRandomArtists());
        model.addAttribute("genres", vRep.selectRandomGenres());
        return "home";
    }

    // Fetches a page with a table of all tracks found from an input trackname
    @RequestMapping(value ="/search", method = RequestMethod.GET)
    public String search(@RequestParam String query, Model model){
        String sanitizedQuery = query.trim();
        model.addAttribute("results", vRep.getTracksByName(sanitizedQuery));
        return "search";
    }

}
