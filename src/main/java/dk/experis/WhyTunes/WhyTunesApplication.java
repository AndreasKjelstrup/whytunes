package dk.experis.WhyTunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhyTunesApplication {

	public static void main(String[] args) {

		SpringApplication.run(WhyTunesApplication.class, args);
	}

}
