package dk.experis.WhyTunes.Models;

public class CustomerSpending {

    // Model-class used for finding the highest spending customers
    private int customerId;
    private String firstName;
    private String lastName;
    private double spending;

    // Default Constructor
    public CustomerSpending() {
    }

    // Constructor
    public CustomerSpending(int customerId, String firstName, String lastName, double spending) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.spending = spending;
    }

    // Getters and Setters
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSpending() {
        return spending;
    }

    public void setSpending(double spending) {
        this.spending = spending;
    }
}
