package dk.experis.WhyTunes.Models;

public class Genre {

    // Model-class used for CustomerGenre
    private String genreName;
    private int genreOccurrence;

    // Default Constructor
    public Genre() {
    }

    // Constructor
    public Genre(String genreName, int genreOccurrence) {
        this.genreName = genreName;
        this.genreOccurrence = genreOccurrence;
    }

    // Getters and Setters
    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getGenreOccurrence() {
        return genreOccurrence;
    }

    public void setGenreOccurrence(int genreOccurrence) {
        this.genreOccurrence = genreOccurrence;
    }
}
