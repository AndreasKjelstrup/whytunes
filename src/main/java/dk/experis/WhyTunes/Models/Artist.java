package dk.experis.WhyTunes.Models;

public class Artist {

    // Model-class used for showing 5 random artists
    private int artistId;
    private String artistName;

    // Default Constructor
    public Artist() {
    }

    // Constructor
    public Artist(int artistId, String artistName) {
        this.artistId = artistId;
        this.artistName = artistName;
    }

    // Getters and Setters
    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
