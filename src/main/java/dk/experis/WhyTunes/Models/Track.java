package dk.experis.WhyTunes.Models;

public class Track {

    // Model-class used for getting 5 random tracks
    int trackID;
    String trackName;

    // Default Constructor
    public Track() {
    }

    // Constructor
    public Track(int trackID, String trackName) {
        this.trackID = trackID;
        this.trackName = trackName;
    }

    // Getters and Setters
    public int getTrackID() {
        return trackID;
    }

    public void setTrackID(int trackID) {
        this.trackID = trackID;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }
}
