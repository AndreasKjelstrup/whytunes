package dk.experis.WhyTunes.Models;

public class ViewGenre {

    // Model-class used for getting 5 random genres
    private int genreId;
    private String genreName;

    // Default Constructor
    public ViewGenre() {
    }

    // Constructor
    public ViewGenre(int genreId, String genreName) {
        this.genreId = genreId;
        this.genreName = genreName;
    }

    // Getters and Setters
    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
