package dk.experis.WhyTunes.Models;

import java.util.ArrayList;

public class CustomerGenre {

    // Model-class used for finding a customers favorite genre
    private int customerId;
    private ArrayList<Genre> genreList;

    // Default Constructor
    public CustomerGenre() {
    }

    // Constructor
    public CustomerGenre(int customerId, ArrayList<Genre> genreList) {
        this.customerId = customerId;
        this.genreList = genreList;
    }

    // Getters and Setters
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public ArrayList<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(ArrayList<Genre> genreList) {
        this.genreList = genreList;
    }

    public void addGenre(Genre genre){
        genreList.add(genre);
    }

}
