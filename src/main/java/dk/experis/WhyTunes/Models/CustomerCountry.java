package dk.experis.WhyTunes.Models;

public class CustomerCountry {

    // Model-class used for listing amount of customers in each country
    private int customerCount;
    private String country;

    // Default Constructor
    public CustomerCountry() {
    }

    // Constructor
    public CustomerCountry(String country, int customers) {
        this.country = country;
        this.customerCount = customers;
    }

    // Getters and Setters
    public int getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(int customerCount) {
        this.customerCount = customerCount;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
